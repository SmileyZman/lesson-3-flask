from flask import Flask

app = Flask(__name__)


@app.route('/requirements/')
def requirements():
    pass


@app.route('/generate-users/')
def generate_users():
    pass


@app.route('/space/')
def space():
    pass


if __name__ == '__main__':
    app.run(debug=True)
